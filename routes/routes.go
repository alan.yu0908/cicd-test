package routes

import (
	"BingoService/model"
	"BingoService/utility"
	"net/http"

	"github.com/gin-gonic/gin"
)

//RouterInit is for initial Routes
func RouterInit() {
	gin.SetMode(gin.ReleaseMode)
	r := gin.New()
	//r := gin.New()
	r.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"status": http.StatusOK})
	})

	r.POST("/BingoService/UpdateBingoBetAmount", utility.Checkjwt, utility.VertifyJwt(), model.UpdateBingoBetAmount)

	r.Run()
}
