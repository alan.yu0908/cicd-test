package protocol

var LvConfig LevelConfig

type level struct {
	Exp         int64     `json:"exp"`
	Rewards     [][]int64 `json:"rewards"`
	TimelyBonus [][]int64 `json:"timely Bonus"`
	AdsBonus    [][]int64 `json:"ads_bouns"`
	MaxBetID    int       `json:"max_bet_id"`
}

type LevelConfig struct {
	Level map[string]level `json:"level"`
}
