package protocol

import "BingoService/common"

type UpdateBingoBerAmountRequest struct {
	GameName     string `json:"gameName"`
	BetAmount    int64  `json:"betAmount"`
	BeforeCredit int64  `json:"beforeCredit"`
	AfterCredit  int64  `json:"afterCredit"`
}
type UpdateBingoBerAmountResponse struct {
	RetStatus    common.RetStatus `json:"retstatus"`
	GameName     string           `json:"gameName"`
	BeforeCredit int64            `json:"beforeCredit"`
	AfterCredit  int64            `json:"afterCredit"`
}

type UpdateBingoTakeWinCreditRequest struct {
	TakeWinCredit int64 `json:"takeWinCredit"`
}

type UpdateBingoTakeWinCreditResponse struct {
	RetStatus    common.RetStatus `json:"retstatus"`
	BeforeCredit int64            `json:"beforeCredit"`
	AfterCredit  int64            `json:"afterCredit"`
}
