package model

const BingoServiceType string = "bingo-service"
const (
	UPDATE_BINGO_BETAMOUNT      string = "UPDATE BINGO BETAMOUNT"
	LOBBY_UPDATE_TAKEWIN_CREDIT string = "LOBBY UPDATE TAKEWIN CREDIT"
	GAME_INIT                   string = "GAME INIT"
)
