package model

import (
	"BingoService/common"
	"BingoService/config"
	"BingoService/protocol"
	"BingoService/utility"
	"strconv"

	"github.com/gin-gonic/gin"
)

const (
	//Database Table usermoney
	GAME_MONEY_QUERY  string = "select game_money,level,user_exp from usermoney where ssid = ? for update"
	GAME_MONEY_UPDATE string = "UPDATE usermoney set game_money=?,level=?,user_exp=? where ssid=?"
	QUERY_MONEY       string = "select game_money from usermoney where ssid = ? for update"
	UPDATE_MONEY      string = "UPDATE usermoney set game_money=? where ssid=?"
	//Database Table achievementTable
	GAME_ACHIEVE_UPDATE string = "UPDATE achievementTable SET game_total_amount=? where ssid=?"
	GAME_ACHIEVE_QUERY  string = "select game_total_amount from achievementTable where ssid = ? for update"
)

//前端update bingo bet acount
func UpdateBingoBetAmount(c *gin.Context) {

	var request protocol.UpdateBingoBerAmountRequest
	var response protocol.UpdateBingoBerAmountResponse
	claim, _ := utility.ParseJwt(c)
	err := c.ShouldBindJSON(&request)
	if err != nil {
		utility.InputErrResponse(c, BingoServiceType, UPDATE_BINGO_BETAMOUNT, claim.SSID, request)
		return
	}
	response.GameName = request.GameName
	//game init
	if request.BetAmount == 0 {

		response.RetStatus.Statuscode = common.RET_CODE_SCUESS
		response.RetStatus.StatusMsg = common.RET_MSG_SCUESS
		utility.ResponseAndSetMongoDB(c, BingoServiceType, GAME_INIT, claim.SSID, request, response)
		return
	}
	if request.AfterCredit < 0 {

		utility.InputErrResponse(c, ServiceType, UPDATE_BINGO_BETAMOUNT, claim.SSID, &request)
		return
	}

	db := utility.GetDBConnection()
	tx, dberr := db.Begin()
	defer func() {
		if p := recover(); p != nil {
			utility.DBErrResponse(c, ServiceType, UPDATE_BINGO_BETAMOUNT, claim.SSID, nil)
			tx.Rollback()
		} else if dberr != nil {
			utility.DBErrResponse(c, ServiceType, UPDATE_BINGO_BETAMOUNT, claim.SSID, nil)
			tx.Rollback()
		} else {
			tx.Commit()

		}

	}()

	var level int
	var game_money, user_exp int64 //user_money table
	var game_total_amount int64    //achievement table

	if dberr = tx.QueryRow(GAME_MONEY_QUERY, claim.SSID).Scan(&game_money, &level, &user_exp); dberr != nil {
		return
	}
	if dberr = tx.QueryRow(GAME_ACHIEVE_QUERY, claim.SSID).Scan(&game_total_amount); dberr != nil {
		return
	}
	response.BeforeCredit = game_money
	if level < config.ConfigGameSetting.Limitmaxlevel { //更新等級還有exp

		user_exp += request.BetAmount

		for i := level + 1; i <= config.ConfigGameSetting.Limitmaxlevel; i++ {
			expGap := protocol.LvConfig.Level[strconv.Itoa(i)].Exp
			if user_exp >= expGap {
				level++
				user_exp -= expGap
			} else {
				break
			}
		}

		if level == config.ConfigGameSetting.Limitmaxlevel {
			user_exp = 0
		}

		//check levelup

	}
	//更新錢
	var differMoney = request.AfterCredit - request.BeforeCredit
	game_money += differMoney
	if game_money < 0 {
		game_money = 0
	}
	response.AfterCredit = game_money
	//更新成就任務面板

	game_total_amount += request.BetAmount

	if _, dberr = tx.Exec(GAME_MONEY_UPDATE, game_money, level, user_exp, claim.SSID); dberr != nil {
		return
	}

	if _, dberr = tx.Exec(GAME_ACHIEVE_UPDATE, game_total_amount, claim.SSID); dberr != nil {
		return
	}

	response.RetStatus.Statuscode = common.RET_CODE_SCUESS
	response.RetStatus.StatusMsg = common.RET_MSG_SCUESS

	utility.ResponseAndSetMongoDB(c, BingoServiceType, UPDATE_BINGO_BETAMOUNT, claim.SSID, request, response)

}

func UpdateBingoTakeWinCredit(c *gin.Context) {

	var request protocol.UpdateBingoTakeWinCreditRequest
	var response protocol.UpdateBingoTakeWinCreditResponse
	claim, _ := utility.ParseJwt(c)
	err := c.ShouldBindJSON(&request)
	if err != nil {
		utility.InputErrResponse(c, BingoServiceType, LOBBY_UPDATE_TAKEWIN_CREDIT, claim.SSID, request)
		return
	}
	if request.TakeWinCredit < 0 {

		utility.InputErrResponse(c, ServiceType, LOBBY_UPDATE_TAKEWIN_CREDIT, claim.SSID, &request)
		return

	}
	var gameMoney int64

	db := utility.GetDBConnection()
	tx, dberr := db.Begin()
	defer func() {
		if p := recover(); p != nil {
			utility.DBErrResponse(c, ServiceType, LOBBY_UPDATE_TAKEWIN_CREDIT, claim.SSID, nil)
			tx.Rollback()
		} else if dberr != nil {
			utility.DBErrResponse(c, ServiceType, LOBBY_UPDATE_TAKEWIN_CREDIT, claim.SSID, nil)
			tx.Rollback()
		} else {
			tx.Commit()

		}

	}()

	if dberr = tx.QueryRow(QUERY_MONEY, claim.SSID).Scan(&gameMoney); dberr != nil {
		return
	}
	response.BeforeCredit = gameMoney

	gameMoney += request.TakeWinCredit
	response.AfterCredit = gameMoney
	if _, dberr = tx.Exec(UPDATE_MONEY, gameMoney, claim.SSID); dberr != nil {
		return
	}
	response.RetStatus.Statuscode = common.RET_CODE_SCUESS
	response.RetStatus.StatusMsg = common.RET_MSG_SCUESS
	utility.ResponseAndSetMongoDB(c, ServiceType, LOBBY_UPDATE_TAKEWIN_CREDIT, claim.SSID, &request, &response)

}
