package utility

import (
	"crypto/md5"
	"crypto/rand"
	"encoding/hex"
	"log"
	"math/big"

	"github.com/sethvargo/go-password/password"
)

//GeneratePassword generatepassword
func GeneratePassword() string {

	res, err := password.Generate(8, 4, 4, false, false)
	if err != nil {

		log.Printf("Password Generate Fail")
		return "Password Generate Fail"
	}
	return res
}

//GeneratePasswordWithMd5 encoding password
func GeneratePasswordWithMd5() string {
	str := GeneratePassword()
	h := md5.New()
	h.Write([]byte(str))
	return hex.EncodeToString(h.Sum(nil))
}

func GenerateRandomNumber(num int64) int64 {
	rnd, _ := rand.Int(rand.Reader, big.NewInt(num))
	return rnd.Int64()
}
