package utility

import (
	"database/sql"
	"fmt"
	"net/http"
	"strconv"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/ip2location/ip2location-go"
)

const (
	ipAdd string = "http://ip-api.com/json/"
)

//IPInfo define IP info
type IPInfo struct {
	Status      string  `json:"status"`
	Country     string  `json:"country"`
	CountryCode string  `json:"countryCode"`
	Region      string  `json:"region"`
	RegionName  string  `json:"regionName"`
	City        string  `json:"city"`
	Zip         string  `json:"zip"`
	Lat         float32 `json:"lat"`
	Lon         float32 `json:"lon"`
	Timezone    string  `json:"timezone"`
	Isp         string  `json:"isp"`
	Org         string  `json:"org"`
	As          string  `json:"as"`
	Query       string  `json:"query"`
}
type setIpError struct {
	IP    string `json:"ip"`
	Error string `json:"err"`
}

//SetIPInformation to DB
func SetIPInformation(ip string, ssid int, db *sql.DB) {
	var dbSSId int64
	err := db.QueryRow("Select ssid from jumbo.user_location_info where ssid=?", ssid).Scan(&dbSSId)
	if err == sql.ErrNoRows {
		result, iperr := GetIpInfo(ip)

		_, iperr = db.Exec("INSERT INTO jumbo.user_location_info (ssid, Country,Region,City,Timezone) VALUES (?,?,?,?,?)", ssid, result.Country_short, result.Region, result.City, result.Timezone)
		if iperr != nil {
			var iperrRecord setIpError
			iperrRecord.IP = ip
			iperrRecord.Error = iperr.Error()
			MongoSetValue("account-service", ssid, "SET IP ERROR", &iperrRecord, nil)
		}
	}
}

//GetTimeZoneinfo GetTimeZoneInfo
func GetTimeZoneinfoOutSide(ip string) string {
	var IPInfo IPInfo
	var url string = ipAdd + ip
	response, err := HttpGetInterFace(url, &IPInfo)
	if response == nil {
		return ""
	}
	if response.StatusCode != http.StatusOK || err != nil {
		fmt.Println("GetTimeZoneError:", err.Error())
		return ""
	}
	return IPInfo.Timezone

}

func ParseTimeZoneInfo(timezone string) (time.Time, *time.Location) {

	index := timezone[0:1]
	hour := timezone[1:3]
	min := timezone[4:6]

	hourint, _ := strconv.Atoi(hour)
	minint, _ := strconv.Atoi(min)
	var offsetSeconds int
	if index == "+" {
		offsetSeconds = hourint*3600 + minint*60
	} else {
		offsetSeconds = 0 - (hourint*3600 + minint*60)
	}

	var GMTZone = time.FixedZone("GMT", offsetSeconds)
	return time.Now().In(GMTZone), GMTZone
}

func GetTimeZoneInfoInside(ip string) (string, error) {
	db, err := ip2location.OpenDB("./IP2LOCATION-LITE-DB11.IPV6.BIN")
	defer db.Close()
	if err != nil {
		return "", err
	}
	results, err := db.Get_all(ip)
	if err != nil {
		return "", err
	}

	return results.Timezone, nil
}

func GetIpInfo(ip string) (ip2location.IP2Locationrecord, error) {
	db, err := ip2location.OpenDB("./IP2LOCATION-LITE-DB11.IPV6.BIN")
	defer db.Close()

	results, err := db.Get_all(ip)

	return results, err
}
