package utility

import (
	"context"
	"crypto"
	"crypto/rsa"
	"crypto/sha1"
	"crypto/x509"
	"database/sql"
	"encoding/base64"
	"encoding/json"

	"github.com/awa/go-iap/appstore"
)

const (
	googlgPublicKey                            = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArjUrBPBI3WDYagUCA6IA1C3u3q0TUl+TsDv/sN+Qg7DRwds2vsX6dyROywJb34izX9z+YjSBFaJBZbazoFmmDy+yFeC44Tzsfxn0oiTSXgvcKC9xl9n4LX920XKDRG81aPHveQGYqTItF1YbUj4VkCdgOHTNCvCxtBPEjepE9lUGcrEkOZH5R5A4mrzSiv/+E/tx8VKGO2xXcm9bE0ZXOj2VuvUGWiGF6ngm5GXIj1m6cFZh2xKuSa248YUFIDhISOaNmhKf8WPSBd/2z5A4ZCqpLGcemttgC85znedFmcLORHlHKNSFMwfMYWTN9ugj6si7FSB/ohR91Bc5xCy9hwIDAQAB"
	appStorePassword                    string = "272d19b4ea4e4000bb48d1542d8ce5da"
	queryPurchaseRecordAndroidByOrderID string = "SELECT no FROM jumbo.iaptableandroid where orderId=?"
	queryPurchaseRecordIOSTracsID       string = "SELECT no FROM jumbo.iaptableios where transaction_id=?"
)

func VertifyGoogleReceipt(recepit string, signature string) error {
	sign, err := base64.StdEncoding.DecodeString(signature)
	if err != nil {
		return err
	}
	public, _ := base64.StdEncoding.DecodeString(googlgPublicKey)
	pub, err := x509.ParsePKIXPublicKey(public)
	if err != nil {
		return err
	}
	hash := sha1.New()
	hash.Write([]byte(recepit))
	return rsa.VerifyPKCS1v15(pub.(*rsa.PublicKey), crypto.SHA1, hash.Sum(nil), sign)
}

func VertifyAppleReceipt(receipt string) (*appstore.IAPResponse, error) {
	client := appstore.New()
	req := appstore.IAPRequest{
		ReceiptData: receipt,
		Password:    appStorePassword,
	}
	resp := &appstore.IAPResponse{}
	ctx := context.Background()
	err := client.Verify(ctx, req, resp)
	return resp, err
}

//GoogleReceipt  is Reciept data
type GoogleReceipt struct {
	ORderID       string `json:"orderId"`
	PackageName   string `json:"packageName"`
	ProductID     string `json:"productId"`
	PurchaseTime  int64  `json:"purchaseTime"`
	PurchaseState int    `json:"purchaseState"`
	PurchaseToken string `json:"purchaseToken"`
}

func CheckGoogleReceiptRepeat(tx *sql.Tx, receipt string, google *GoogleReceipt) error {
	var no int

	json.Unmarshal([]byte(receipt), google)
	err := tx.QueryRow(queryPurchaseRecordAndroidByOrderID, google.ORderID).Scan(&no)
	return err
}

func CheckIOSReceiptRepeat(tx *sql.Tx, TransactionID string) error {
	var no int

	err := tx.QueryRow(queryPurchaseRecordIOSTracsID, TransactionID).Scan(&no)
	return err
}
