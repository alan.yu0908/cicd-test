package utility

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

//Get FB Token
const (
	FbTokenAddress string = "https://graph.facebook.com/me?access_token="

	appleID        string = "496807127336784"
	appSecret      string = "b4185097389cdc0613f2775c61d78a36"
	appAccesstoken string = "496807127336784|HM8nm_ll9RASBwB1SITDOV84dnw"
)

type UserInfo struct {
	Name string `json:"name"`
	ID   string `json:"id"`
}

type FBlongToken struct {
	AccessToken string `json:"access_token"`
	TokenType   string `json:"token_type"`
	ExpiresTime string `json:"expires_in"`
}

type fBTokenCheck struct {
	BackData data `json:"data"`
}

type data struct {
	ISVaild bool   `json:"is_valid"`
	UserID  string `json:"user_id"`
}

//GetUserInfobyFBToken return name:id
func GetUserInfobyFBToken(token string) (string, string) {

	var url string = FbTokenAddress + token
	response, err := http.Get(url)

	if err != nil {

		fmt.Println(err.Error())
		return err.Error(), ""
	}
	defer response.Body.Close()
	var userinfo UserInfo
	body, err := ioutil.ReadAll(response.Body)
	json.Unmarshal(body, &userinfo)

	return userinfo.Name, userinfo.ID

}

//GetUserLongToekn  GetUserLongToekn
func GetUserLongToekn(token string) string {
	var url string = "https://graph.facebook.com/oauth/access_token?client_id=" + appleID + "&client_secret=" + appSecret + "&grant_type=fb_exchange_token&fb_exchange_token=" + token
	response, err := http.Get(url)

	if err != nil {
		fmt.Printf(err.Error())
		return err.Error()
	}
	defer response.Body.Close()
	var tokeninfo FBlongToken
	body, err := ioutil.ReadAll(response.Body)
	json.Unmarshal(body, &tokeninfo)

	return tokeninfo.AccessToken
}

//CheckFBTokenValid check fbToken is vaild
func CheckFBTokenValid(fbToken string) bool {
	var url string = "https://graph.facebook.com/debug_token?input_token=" + fbToken + "&access_token=" + appAccesstoken
	response, err := http.Get(url)
	if err != nil {
		fmt.Printf(err.Error())
		return false
	}
	defer response.Body.Close()
	var tokenCheck fBTokenCheck
	body, err := ioutil.ReadAll(response.Body)
	json.Unmarshal(body, &tokenCheck)
	return tokenCheck.BackData.ISVaild
}
