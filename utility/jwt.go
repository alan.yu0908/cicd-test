package utility

//jwt url
const (
	K8sURL = "http://jwt-service:8080/jwtService/Create"
)

//GetJwt GetJWT
func GetJwt(ssid int, account string, platform string, timezone string, version string) (string, error) {
	jwtReq := jwtRequest{ssid, account, platform, timezone, version}
	var jwt JwtRes
	err := HttpPostInterFace(K8sURL, &jwtReq, &jwt)
	return jwt.JWT, err
}

type jwtRequest struct {
	SSID     int    `json:"ssid"`
	Account  string `json:"account"`
	Platform string `json:"platform"`
	TimeZone string `json:"timezone"`
	Version  string `json:"version"`
}

//JwtRes is for JWTCreate
type JwtRes struct {
	Retstatus RetStatus `json:"retstatus"`
	Account   string    `json:"account"`
	JWT       string    `json:"jwt"`
}

//RET_MSG_JWT_GET_JWT_ERROR  string = "GET_JWT_ERROR"
//JWT ERROR CODE
const (
	RET_CODE_JWT_GET_JWT_ERROR int = 1700
	RET_CODE_SET_REDIS_ERROR   int = 1701
	RET_CODE_GET_REDIS_ERROR   int = 1702
	RET_CODE_RELOGIN           int = 1703
)

const (
	RET_MSG_GET_JWT_ERROR   string = "GET JWT ERROR"
	RET_MSG_SET_REDIS_ERROR string = "SET REDIS ERROR"
	RET_MSG_GET_REDIS_ERROR string = "GET REDIS ERROR"
	RET_MSG_RELOGIN         string = "RELOGIN"
)

//tokern message
const (
	RET_CODE_MALFORMED      int = 1704
	RET_CODE_NOT_VERIFIED   int = 1705
	RET_CODE_SIGNATURE_FAIL int = 1706
	RET_CODE_EXPIRED        int = 1707
	RET_CODE_NOTTIME        int = 1708
	RET_CODE_NOT_HANDLE     int = 1709
	RET_CODE_UNKNOW         int = 1710
)

//toker message
const (
	RET_MSG_MALFORMED      string = "TOKEN IS MALFORMED "
	RET_MSG_NOT_VERIFIED   string = "TOEKN COUND NOT BE VERIFIED"
	RET_MSG_SIGNATURE_FAIL string = "SIGNATURE VALIDATION FAILED"
	RET_MSG_EXPIRED        string = "TOKEN IS EXPIRED"
	RET_MSG_NOTTIME        string = "TOKEN IS NOT YET VALID BEFORE SOMETIME"
	RET_MSG_NOT_HANDLE     string = "CAN HOT HANDLE THIS TOKEN"
	RET_MSG_UNKNOW         string = "UNKNOW ERR"
)
