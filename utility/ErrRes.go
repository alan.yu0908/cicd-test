package utility

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

//1100~1200 DB
const (
	RET_CODE_DB_ACCESS_ERROR int = 1100

	DB_MSG_ACCESS_ERROR      string = "DB ACCESS ERROR"
	RET_CODE_INPUT_ERROR     int    = 1001
	RET_CODE_GETCONFIG_ERROR int    = 1002
	RET_MSG_INPUT_ERROR      string = "INPUT DATA ERROR"
	RET_MSG_GETCONFIG_ERROR  string = "GET CONFIG_ERROR"
)

type RetStatus struct {
	Statuscode int    `json:"statuscode"`
	StatusMsg  string `json:"statusMsg"`
}
type ErrorResPonse struct {
	RetStatus RetStatus `json:"retStatus"`
}

func DBErrResponse(c *gin.Context, servicetype string, requestType string, ssid int, request interface{}) {

	var errRes ErrorResPonse
	errRes.RetStatus.Statuscode = RET_CODE_DB_ACCESS_ERROR
	errRes.RetStatus.StatusMsg = DB_MSG_ACCESS_ERROR
	c.JSON(http.StatusOK, errRes)
	MongoSetErrorValue(servicetype, requestType, ssid, request, errRes)

}

func InputErrResponse(c *gin.Context, servicetype string, requestType string, ssid int, request interface{}) {

	var errRes ErrorResPonse
	errRes.RetStatus.Statuscode = RET_CODE_INPUT_ERROR
	errRes.RetStatus.StatusMsg = RET_MSG_INPUT_ERROR
	c.JSON(http.StatusOK, errRes)
	MongoSetErrorValue(servicetype, requestType, ssid, request, errRes)
}

func GetConfigErrResponse(c *gin.Context, servicetype string, requestType string, ssid int, request interface{}) {

	var errRes ErrorResPonse
	errRes.RetStatus.Statuscode = RET_CODE_GETCONFIG_ERROR
	errRes.RetStatus.StatusMsg = RET_MSG_GETCONFIG_ERROR
	c.JSON(http.StatusOK, errRes)
	MongoSetErrorValue(servicetype, requestType, ssid, request, errRes)
}
