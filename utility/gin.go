package utility

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

//ResponseAndSetMongoDB response and save mongoData
func ResponseAndSetMongoDB(c *gin.Context, serviceType string, requestType string, ssid int, request interface{}, response interface{}) {
	//func ResponseAndSetMongoDB(c *gin.Context, database string, collection string, ssid int, requestType string, request interface{}, response interface{}) {
	c.JSON(http.StatusOK, response)
	MongoSetValue(serviceType, ssid, requestType, request, response)
}

//ResponseAndSetMongoDB Errorresponse and save mongoData
func ResponseErrorAndSetMongoDB(c *gin.Context, statusCode int, serviceType string, requestType string, ssid int, request interface{}, response interface{}) {
	//func ResponseAndSetMongoDB(c *gin.Context, database string, collection string, ssid int, requestType string, request interface{}, response interface{}) {
	c.JSON(statusCode, response)
	MongoSetErrorValue(serviceType, requestType, ssid, request, response)
}
