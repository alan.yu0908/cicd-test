package utility

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

const timeFormate string = "2006-01-02 15:04:05"
const TimezoneTaipei string = "+08:00"
const RequestCollection string = "Request"
const ErrorCollection string = "ErrorResPonse"

// //MongoRecord Save Type for MongoDB
// type MongoRecord struct {
// 	SSID           int    `json:"SSID"`
// 	RequestType    string `json:"RequestType"`
// 	RequestEntity  string `json:"RequestEntity"`
// 	ResponseEntity string `json:"ResponseEntity"`
// 	TimeStamp      int64  `json:"TimeStamp"`
// 	TimeFormate    string `json:"TimeFormat"`
// }

//var mongoClient *mongo.Client
var mongoAtlas *mongo.Client
var mongoClient *mongo.Client
var atlasErr error

//GCP Local IP
//var mongourl string = "mongodb://root:rYz98dwf7kwC8WBnz9KFQ9Suy5pm2YY7@10.140.0.19:27017/?authSource=admin"

//extern IP
//var mongourl string = "mongodb://root:rYz98dwf7kwC8WBnz9KFQ9Suy5pm2YY7@104.199.137.14:27017/?authSource=admin"

func ConnectMongoDB() {
	mongoClient, _ = mongo.NewClient(options.Client().ApplyURI(Mongourl))
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	err := mongoClient.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}
}

func ConnectMongoAtlas() {

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	mongoAtlas, atlasErr = mongo.Connect(ctx, options.Client().ApplyURI(
		"mongodb+srv://alphabet:Alphabet.com@cluster-bingo-pri.fdrkr.mongodb.net/?retryWrites=true&w=majority",
	))
	if atlasErr != nil {
		fmt.Printf(atlasErr.Error())
	}

	// Ping the primary
	if atlasErr = mongoAtlas.Ping(ctx, readpref.Primary()); atlasErr != nil {
		fmt.Printf(atlasErr.Error())
	} else {
		fmt.Println("Successfully connected and pinged.")

	}

}

func MongoSetValue(serviceType string, ssid int, requestType string, request interface{}, response interface{}) {

	var RequestEntity string
	var ResponseEntity string
	var timestamp int64

	Currentime := time.Now()
	timelocal, _ := ParseTimeZoneInfo(TimezoneTaipei)

	if request != nil {
		requestdata, _ := json.Marshal(request)
		RequestEntity = string(requestdata)
	}
	if response != nil {
		responsedata, _ := json.Marshal(response)
		ResponseEntity = string(responsedata)
	}

	timestamp = Currentime.Unix()

	Local := timelocal.Format(timeFormate)
	ctx, _ := context.WithTimeout(context.Background(), 2*time.Second)
	Moncollection := mongoClient.Database(serviceType).Collection(requestType)
	_, err := Moncollection.InsertOne(ctx, bson.M{"ssid": ssid, "RequestType": requestType, "Request": RequestEntity, "Response": ResponseEntity, "TimeStamp": timestamp, "LocalTime": Local})
	if err != nil {
		fmt.Println(err.Error())
		record := fmt.Sprintf("ssid:%d,RequestType:%s,Request:%s,Response:%s,TimeStamp:%d,LocalTime:%s", ssid, requestType, request, response, timestamp, Local)
		fmt.Println(record)
	}

	if atlasErr == nil {
		ctxAtlas, _ := context.WithTimeout(context.Background(), 2*time.Second)
		mognoAtlasCollection := mongoAtlas.Database(serviceType).Collection(requestType)
		_, errAtlas := mognoAtlasCollection.InsertOne(ctxAtlas, bson.M{"ssid": ssid, "RequestType": requestType, "Request": RequestEntity, "Response": ResponseEntity, "TimeStamp": timestamp, "LocalTime": Local})

		if errAtlas != nil {
			fmt.Println(errAtlas.Error())
			record := fmt.Sprintf("ssid:%d,RequestType:%s,Request:%s,Response:%s,TimeStamp:%d,LocalTime:%s", ssid, requestType, request, response, timestamp, Local)
			fmt.Println(record)
		}
	}

}

func MongoSetErrorValue(serviceType string, requestType string, ssid int, request interface{}, response interface{}) {

	var RequestEntity string
	var ResponseEntity string
	var timestamp int64

	Currentime := time.Now()
	timelocal, _ := ParseTimeZoneInfo(TimezoneTaipei)

	if request != nil {
		requestdata, _ := json.Marshal(request)
		RequestEntity = string(requestdata)
	}
	if response != nil {
		responsedata, _ := json.Marshal(response)
		ResponseEntity = string(responsedata)
	}

	timestamp = Currentime.Unix()

	Local := timelocal.Format(timeFormate)
	ctx, _ := context.WithTimeout(context.Background(), 2*time.Second)

	Moncollection := mongoClient.Database(serviceType).Collection(ErrorCollection)
	_, err := Moncollection.InsertOne(ctx, bson.M{"ssid": ssid, "RequestType": requestType, "Request": RequestEntity, "Response": ResponseEntity, "TimeStamp": timestamp, "LocalTime": Local})
	if err != nil {
		fmt.Println(err.Error())
		record := fmt.Sprintf("ssid:%d,RequestType:%s,Request:%s,Response:%s,TimeStamp:%d,LocalTime:%s", ssid, requestType, request, response, timestamp, Local)
		fmt.Println(record)
	}

	if atlasErr == nil {
		ctxAtlas, _ := context.WithTimeout(context.Background(), 2*time.Second)
		mognoAtlasCollection := mongoAtlas.Database(serviceType).Collection(requestType)
		_, errAtlas := mognoAtlasCollection.InsertOne(ctxAtlas, bson.M{"ssid": ssid, "RequestType": requestType, "Request": RequestEntity, "Response": ResponseEntity, "TimeStamp": timestamp, "LocalTime": Local})

		if errAtlas != nil {
			fmt.Println(errAtlas.Error())
			record := fmt.Sprintf("ssid:%d,RequestType:%s,Request:%s,Response:%s,TimeStamp:%d,LocalTime:%s", ssid, requestType, request, response, timestamp, Local)
			fmt.Println(record)
		}
	}

}
