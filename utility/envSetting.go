package utility

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
)

//GetEnvSetting to get evn
func GetEnvSetting(envSetting map[string]string, para []string) {

	err := godotenv.Load()
	if err != nil {
		fmt.Printf("Error loading .env file")
	}

	for _, e := range para {
		envSetting[e] = os.Getenv(e)
	}
}
