package utility

import (
	"database/sql"
	"fmt"
	"log"
	"sync"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

var dbcontroller *sql.DB
var gameHistoryConnectionOnce_wr sync.Once

func GetDBConnection() *sql.DB {

	return dbcontroller
}

func InitDBConnection(maxOpenConns int, maxIdleConns int) {
	gameHistoryConnectionOnce_wr.Do(func() {
		db, err := sql.Open("mysql", Dburl)
		if err != nil {
			panic(err)
		}
		err = db.Ping()
		if err != nil {
			panic(err)
		}
		db.SetConnMaxLifetime(180)
		db.SetMaxIdleConns(maxIdleConns)
		db.SetMaxOpenConns(maxOpenConns)
		dbcontroller = db
	})
}

var DBTimeOut time.Duration = time.Second
var Dbuser string
var Dbpas string
var Dburl string
var Mongourl string
var JWT_CODE string
var STORAGE_POS string
var APP_VERSION string
var MONGO_IP string
var MONGO_USER string
var MONGO_PASS string

//config paramter
const (
	//DBURL string = "%s:%s@tcp(35.194.203.221:3306)/jumbo?parseTime=true"
	DBURL      string = "%s:%s@tcp(127.0.0.1:3306)/jumbo?parseTime=true"
	MONGOURL   string = "mongodb://%s:%s@%s:27017/?authSource=admin"
	DEBUGMode  string = "DEBUG"
	DBUSER     string = "DBUSER"
	DBPASSWORD string = "DBPASSWORD"
	JWTCODE    string = "JWTCODE"
	STORAGEPOS string = "STORAGEPOS"
	VERSION    string = "VERSION"
	MONGOIP    string = "MONGOIP"
	MONGOUSER  string = "MONGOUSER"
	MONGOPASS  string = "MONGOPASS"
)

var EnvParameter = []string{DEBUGMode, DBUSER, DBPASSWORD, JWTCODE, STORAGEPOS, VERSION, MONGOIP, MONGOUSER, MONGOPASS}
var EnvSetting map[string]string
var Debug bool

func DBGetEnvSetting() {
	EnvSetting = make(map[string]string)
	GetEnvSetting(EnvSetting, EnvParameter)
	if EnvSetting[DEBUGMode] == "1" {
		Debug = true
	} else {
		Debug = false
	}
	Dbuser = EnvSetting[DBUSER]
	Dbpas = EnvSetting[DBPASSWORD]
	Dburl = fmt.Sprintf(DBURL, Dbuser, Dbpas)
	JWT_CODE = EnvSetting[JWTCODE]
	STORAGE_POS = EnvSetting[STORAGEPOS]
	APP_VERSION = EnvSetting[VERSION]
	MONGO_IP = EnvSetting[MONGOIP]
	MONGO_USER = EnvSetting[MONGOUSER]
	MONGO_PASS = EnvSetting[MONGOPASS]
	Mongourl = fmt.Sprintf(MONGOURL, MONGO_USER, MONGO_PASS, MONGO_IP)

}

//GetDBRowsData return map for DB raws data
func GetDBRowsData(rows *sql.Rows) []map[string]string {
	if rows == nil {
		fmt.Printf(" Rows Err")
		return nil
	}
	var results []map[string]string
	cols, _ := rows.Columns()

	vals := make([][]byte, len(cols))
	scans := make([]interface{}, len(cols))

	for i := range vals {
		scans[i] = &vals[i]
	}

	for rows.Next() {
		err := rows.Scan(scans...)
		if err != nil {
			log.Fatalln(err)
		}

		row := make(map[string]string)
		for k, v := range vals {
			key := cols[k]
			row[key] = string(v)
		}
		results = append(results, row)
	}

	return results
}
