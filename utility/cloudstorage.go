package utility

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"

	"cloud.google.com/go/storage"
	"google.golang.org/api/option"
)

func GetAuthenticationConfig(version string, fileName string, object interface{}) error {

	ctx := context.Background()
	client, err := storage.NewClient(ctx)

	if err != nil {
		fmt.Println(err)
		return err
	}
	bkt := client.Bucket(STORAGE_POS)
	var data string
	if version == "" {
		data = fileName
	} else {
		data = version + "/" + fileName
	}

	obj := bkt.Object(data)
	// Read it back.
	r, err := obj.NewReader(ctx)
	defer r.Close()
	if err != nil {
		fmt.Println(err)
		return err
	}
	slurp, err := ioutil.ReadAll(r)
	err = json.Unmarshal(slurp, object)
	return err

}

func GetWithoututhenticationConfig(version string, fileName string, object interface{}) error {

	ctx := context.Background()
	//client, err := storage.NewClient(ctx)
	client, err := storage.NewClient(ctx, option.WithoutAuthentication())
	if err != nil {
		fmt.Println(err)
		return err
	}
	bkt := client.Bucket("api-project-544726660757.appspot.com")
	var data string

	if version == "" {
		data = fileName
	} else {
		data = version + "/" + fileName
	}
	obj := bkt.Object(data)
	// Read it back.
	r, err := obj.NewReader(ctx)
	defer r.Close()
	if err != nil {
		fmt.Println(err)
	}
	slurp, err := ioutil.ReadAll(r)
	fmt.Printf(string(slurp))
	err = json.Unmarshal(slurp, object)
	return err

}
