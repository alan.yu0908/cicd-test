package utility

import (
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

//Claims is for Payload
type Claims struct {
	SSID     int    `json:"ssid"`
	Account  string `json:"account"`
	Role     string `json:"role"`
	Platform string `json:"platform"`
	TimeZone string `json:"timezone"`
	Version  string `json:"version"`
	jwt.StandardClaims
}

//token 密鑰，之後應該要設定在環境變數中

//Checkjwt jwt middleware
func Checkjwt(c *gin.Context) {
	var jwtSecret = []byte(JWT_CODE)
	jwtToken := c.GetHeader("Authorization")
	_, err := jwt.ParseWithClaims(jwtToken, &Claims{}, func(token *jwt.Token) (i interface{}, err error) {
		return jwtSecret, nil
	})

	if err != nil {
		var response jwtResponse
		var message string
		var code int
		if ve, ok := err.(*jwt.ValidationError); ok {

			if ve.Errors&jwt.ValidationErrorMalformed != 0 {
				message = RET_MSG_MALFORMED
				code = RET_CODE_MALFORMED
			} else if ve.Errors&jwt.ValidationErrorUnverifiable != 0 {
				message = RET_MSG_NOT_VERIFIED
				code = RET_CODE_NOT_VERIFIED
			} else if ve.Errors&jwt.ValidationErrorSignatureInvalid != 0 {
				message = RET_MSG_SIGNATURE_FAIL
				code = RET_CODE_SIGNATURE_FAIL
			} else if ve.Errors&jwt.ValidationErrorExpired != 0 {
				message = RET_MSG_EXPIRED
				code = RET_CODE_EXPIRED
			} else if ve.Errors&jwt.ValidationErrorNotValidYet != 0 {
				message = RET_MSG_NOTTIME
				code = RET_CODE_NOTTIME
			} else {
				message = RET_MSG_NOT_HANDLE
				code = RET_CODE_NOT_HANDLE
			}
		}
		response.Retstatus.StatusMsg = message
		response.Retstatus.Statuscode = code
		c.JSON(http.StatusOK, response)
		c.Abort()

	} else {

		c.Next()
	}

}

//ParseJwt ParseJwt
func ParseJwt(c *gin.Context) (*Claims, error) {
	var jwtSecret = []byte(JWT_CODE)
	jwtToken := c.GetHeader("Authorization")
	token, err := jwt.ParseWithClaims(jwtToken, &Claims{}, func(token *jwt.Token) (i interface{}, err error) {
		return jwtSecret, nil
	})

	if claims, ok := token.Claims.(*Claims); ok && token.Valid {
		//if claims, ok := token.Claims.(*Claims); ok {

		return claims, nil
	}
	return nil, err

}

type jwtResponse struct {
	Retstatus RetStatus `json:"retstatus"`
}
