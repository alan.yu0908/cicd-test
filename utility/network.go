package utility

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

const (
	//jwtServiceDebug string = "https://lobby.bingotayodev.com/jwtService/Vertify"
	jwtServiceURL string = "http://jwt-service:8080/jwtService/Vertify"
)

func HttpPost(url string, json []byte) []byte {

	c := http.Client{
		Timeout: 1 * time.Second,
	}

	sendData := bytes.NewBuffer(json)
	request, _ := http.NewRequest("POST", url, sendData)
	request.Header.Set("Content-Type", "application/json;charset=UTF-8")
	res, _ := c.Do(request)
	body, _ := ioutil.ReadAll(res.Body)
	return body

}

func HttpPostInterFace(url string, request interface{}, response interface{}) error {

	c := http.Client{
		Timeout: 1 * time.Second,
	}

	data, err := json.Marshal(request)
	sendData := bytes.NewBuffer(data)
	httprequest, _ := http.NewRequest("POST", url, sendData)
	httprequest.Header.Set("Content-Type", "application/json;charset=UTF-8")
	res, err := c.Do(httprequest)
	if err != nil {
		return err
	}
	body, err := ioutil.ReadAll(res.Body)
	err = json.Unmarshal(body, response)
	return err
	//Sample
	// request := make(utility.CommonApiRequest, 1)
	// var commonRes utility.CommonApiResponse
	// request[0].Ssid = 1
	// request[0].Account = "BG10000001"
	// request[0].Betamount = 50
	// request[0].Gamewin = 1000

	// utility.HttpPostInterFace("http://34.120.241.26:80/Common", &request, &commonRes)
	// println(commonRes.Account)
}

func HttpGetInterFace(url string, response interface{}) (*http.Response, error) {

	c := http.Client{
		Timeout: 1 * time.Second,
	}
	httprequest, _ := http.NewRequest("Get", url, nil)
	res, err := c.Do(httprequest)
	if err != nil || res.StatusCode != http.StatusOK {
		return res, err
	}
	body, err := ioutil.ReadAll(res.Body)
	defer res.Body.Close()
	err = json.Unmarshal(body, response)
	return res, err
}

func VertifyJwt() gin.HandlerFunc {
	return func(c *gin.Context) {
		var jwtRes jwtResponse
		jwt := c.GetHeader("Authorization")
		clinet := http.Client{
			Timeout: 1 * time.Second,
		}
		httprequest, _ := http.NewRequest("POST", jwtServiceURL, nil)
		httprequest.Header.Set("Authorization", jwt)
		res, _ := clinet.Do(httprequest)
		body, _ := ioutil.ReadAll(res.Body)
		json.Unmarshal(body, &jwtRes)
		if jwtRes.Retstatus.Statuscode != 1000 {
			c.JSON(http.StatusOK, jwtRes)
			c.Abort()
			return

		}
		c.Next()
	}
}
