package config

//ConfigSetting ConfigSetting
var ConfigSetting GameConfig
var ConfigGameSetting GameSetting

//config paramter
const (
	ConfigName            string = "config.json"
	GamesettingName       string = "gamesetting.json"
	RewardLevelConfigName string = "RewardLevelConfig.json"
	DailyRewardConfigName string = "dailyRewardConfig.json"
	ActivityRewardName    string = "activityReward.json"
)

type GameConfig struct {
	ADPlayInterval      int     `json:"ADPlayInterval"`
	ExpTipCloseTime     int     `json:"ExpTipCloseTime"`
	WatchADIntervelTime float64 `json:"WatchADIntervelTime"`
	DefaultAdsCount     int     `json:"DefaultAdsCount"`
	IconAppear          int     `json:"IconAppear"`
	FriendPanel         int     `json:"FriendPanel"`
	ShoPanel            int     `json:"ShoPanel"`
	MailPanel           int     `json:"MailPanel"`
	DailyAwardPanel     int     `json:"DailyAwardPanel"`
	TimedAward          int     `json:"TimedAward"`
	AndroidAppID        string  `json:"AndroidAppID"`
	IosAppID            string  `json:"IosAppID"`
	AndroidUnitID       string  `json:"AndroidUnitID"`
	IosUnitID           string  `json:"IosUnitID"`
	AndroidUnitIDTest   string  `json:"AndroidUnitIDTest"`
	IosUnitIDTest       string  `json:"IosUnitIDTest"`
}
type GameSetting struct {
	//	[names]
	//	;maintain是否維護(0:未開啟維護,1:開啟維護)
	Maintain       int      `json:"maintain"`
	Whitelisting   []string `json:"whitelisting"`
	MaintainStr    string   `json:"maintainStr"`    //"Maintain \"  \" Time! '\n' \"  \"Please\"  \" try\"  \" again\"  \" later\"  \" Thanks."
	Version        string   `json:"version"`        //"1.1.3"
	LastVersion    string   `json:"lastVersion"`    //1.1.2
	Startgamemoney int64    `json:"startgamemoney"` // "2500"
	Fbrewards      int64    `json:"fbrewards"`      //"2500"
	Timerewardsgap int      `json:"timerewardsgap"` // "4"
	Friendsmax     int      `json:"friendsmax"`     // "200"
	Timeupdate     int      `json:"timeupdate"`     // "3"
	Totalbetmax    int64    `json:"totalbetmax"`    // "1000000"
	Invitebonus    int      `json:"invitebonus"`    // "100"
	Gameupdate     int      `json:"gameupdate"`     // "300"
	Coupontime     int      `json:"coupontime"`     // "180"
	Couponmoney    int64    `json:"couponmoney"`    // "60000"
	//;Coupon活動類型(coupon=0,gift=1,credit=2)
	Coupontype      int `json:"coupontype"`      // "2"
	Newlucklevel    int `json:"newlucklevel"`    //"9"
	Newluckbouns    int `json:"newluckbouns"`    //"200"
	Couponmultiple  int `json:"couponmultiple"`  //"100"
	Luclywildstart  int `json:"luclywildstart"`  //"5"
	Luclywildend    int `json:"luclywildend"`    //"20"
	Normalwildstart int `json:"normalwildstart"` //"100"
	Normalwildend   int `json:"normalwildend"`   //"200"
	Limitmaxlevel   int `json:"limitmaxlevel"`   //25
	//;mail多久更新一次(秒)
	Mailupdate          int64  `json:"mailupdate"` //86400
	MailRewardStartTime string `json:"mailRewardStartTime"`
	MailRewardEndTime   string `json:"mailRewardEndTime"`
	MailID              int    `json:"mailID"`
	//[urls]
	Updateiosurl     string `json:"updateiosurl"`     // "https://itunes.apple.com/us/app/bingotayo/id1271292759"
	Updateandroidurl string `json:"updateandroidurl"` // "https://play.google.com/store/apps/details?id //com.Alphabet.BingoTayo"
	Newversionurl    string `json:"newversionUrl"`    //"https://www.bingotayo.com/bingo/updateapp.php";
	//	[GameRtp]
	//	;RTP參數(base Rtp,新手好運extra Rtp ,一般extra Rtp)
	GameRtp gamertp `json:"gamertp"`
}

type gamertp struct {
	EzBingo         string `json:"EzBingo"`         // "90&1&1&1"
	VideoBingo      string `json:"VideoBingo"`      // "90&&1&1"
	Lucky8          string `json:"Lucky8"`          // "90&98&90"
	DW              string `json:"DW"`              // "90&1&1"
	Atlantis        string `json:"Atlantis"`        // "88&98&90"
	OpenSesame      string `json:"OpenSesame"`      // "88&98&90"
	RoosterChampion string `json:"RoosterChampion"` // "88&1&1"
	CandyLand       string `json:"CandyLand"`       // "90&1&1"
	DancingPapa     string `json:"DancingPapa"`     // "90&98&90"
	OpenSesame2     string `json:"OpenSesame2"`     // "90&1&1"
	WinCaiShen      string `json:"WinCaiShen"`      //  "88&98&90"
}
