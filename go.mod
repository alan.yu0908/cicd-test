module BingoService

go 1.14

require (
	cloud.google.com/go/storage v1.12.0
	github.com/awa/go-iap v1.3.5
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.6.3
	github.com/go-sql-driver/mysql v1.5.0
	github.com/ip2location/ip2location-go v8.3.0+incompatible
	github.com/joho/godotenv v1.3.0
	github.com/sethvargo/go-password v0.2.0
	go.mongodb.org/mongo-driver v1.4.4
	google.golang.org/api v0.36.0
)
