 #GO Repo base repo
FROM golang:1.14-alpine as builder
ENV GO111MODULE=on
RUN apk add git
# Add Maintainer Info
LABEL maintainer="<>"
RUN mkdir /app
ADD . /app
WORKDIR /app
COPY go.mod go.sum ./
# Download all the dependencies
RUN go mod download
COPY . .
# Build the Go app
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .
# GO Repo base repo
FROM alpine:latest
RUN apk --no-cache add ca-certificates curl
RUN mkdir /app
WORKDIR /app/
# Copy the Pre-built binary file from the previous stage
COPY --from=0 /usr/local/go/lib/time/zoneinfo.zip /opt/zoneinfo.zip
ADD IP2LOCATION-LITE-DB11.IPV6.BIN IP2LOCATION-LITE-DB11.IPV6.BIN
ENV ZONEINFO /opt/zoneinfo.zip
COPY --from=builder /app/main .


# Expose port 8000
EXPOSE 8080
# Run Executable
CMD ["/app/main"]
#ENTRYPOINT ./coderminer