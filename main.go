package main

import (
	"BingoService/config"
	"BingoService/protocol"
	"BingoService/routes"
	"BingoService/utility"
)

func main() {
	utility.DBGetEnvSetting()
	setConfigSetting()
	utility.ConnectMongoDB()
	utility.ConnectMongoAtlas()
	utility.InitDBConnection(30, 30)
	routes.RouterInit()

}

func setConfigSetting() {

	err := utility.GetAuthenticationConfig("", "RewardLevelConfig.json", &protocol.LvConfig)
	//err := utility.GetWithoututhenticationConfig("", "RewardLevelConfig.json", &protocol.LvConfig)
	if err != nil {
		panic(err.Error())
	}
	if err = utility.GetAuthenticationConfig("", config.GamesettingName, &config.ConfigGameSetting); err != nil {
		//if err = utility.GetWithoututhenticationConfig("", config.GamesettingName, &config.ConfigGameSetting); err != nil {
		panic(err.Error())
	}

}
